<?php
/* USAGE:
$v = boost()->validation->data(array('name'=>'Adam', 'email'=>'johnsmith@gmail.com'));
$v->rule('required', 'name');
$v->rule('email', 'email');

if($v->validate()) {
    echo "We're all good!";
} else {
    // Errors
    print_r($v->errors());
}
*/

namespace Boost;

boost()->add_callable('validation', 'Boost\Validation');

Class Validation extends Library {

	function data($data_to_validate) {
		return new \Valitron\Validator($data_to_validate);
	}

}